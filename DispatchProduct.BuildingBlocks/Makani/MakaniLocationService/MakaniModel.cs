﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MakaniLocationService
{
    public class MakaniModel
    {
        public string MAKANI { get; set; }
        public string LAT { get; set; }
        public string LNG { get; set; }
        public string ENT_NAME_E { get; set; }
        public string ENT_NAME_A { get; set; }
        public string ENT_NO { get; set; }
        public string SHORT_URL { get; set; }
        public string COMMUNITY_E { get; set; }
        public string COMMUNITY_A { get; set; }
        public string BLDG_NAME_E { get; set; }
        public string BLDG_NAME_A { get; set; }
        public string EMIRATE_E { get; set; }
        public string EMIRATE_A { get; set; }
    }
}
