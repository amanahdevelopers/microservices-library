﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CommonEnums
{
    public enum ReviewItems
    {
        [Description("التطبيق")]
        Application = 1,
        [Description("الطلب")]
        Order = 2,
        [Description("شكوى")]
        Complain = 3,
        [Description("تواصل معنا")]
        ContactUs = 4
    }
}
