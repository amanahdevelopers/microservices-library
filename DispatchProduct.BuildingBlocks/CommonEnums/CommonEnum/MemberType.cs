﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonEnums
{
    public enum MemberType
    {
        Technician = 1,
        Driver = 2 ,
        Vehicle = 3
    }
}
