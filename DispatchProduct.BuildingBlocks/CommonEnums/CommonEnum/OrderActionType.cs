﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonEnum
{
    public enum OrderActionType
    {
        Acceptence = 1,
        Rejection = 2,
        ChangeStatus = 3,
        BulkAssign = 4,
        BulkUnAssign = 5,
        Assign = 6,
        UnAssgin = 7,
        AddOrder = 8,
        ChangeTeamRank = 9,
        Break=10,
        Idle=11,
        Work=12,
        FirstOrderWork = 13,
        UpdateOrder = 14
    }
}
