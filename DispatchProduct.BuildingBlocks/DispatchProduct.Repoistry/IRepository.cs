﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;

namespace DispatchProduct.RepositoryModule
{
    public interface IRepository<TEntity> where TEntity : class, IBaseEntity
    {
        ProcessResult<IQueryable<TEntity>> GetAllQuerable();
        ProcessResult<IQueryable<TEntity>> GetAllQuerableWithBlocked();
        ProcessResult<List<TEntity>> GetAll();
        ProcessResult<List<TEntity>> GetAll(Expression<Func<TEntity, bool>> predicate);
        ProcessResult<List<TEntity>> GetAllWithBlocked();
        ProcessResult<List<TEntity>> GetAllWithBlocked(Expression<Func<TEntity, bool>> predicate);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginatedWithBlocked(PaginatedItems<TEntity> paginatedItems);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginatedWithBlocked<TKey>(PaginatedItems<TEntity> paginatedItems, Func<TEntity, TKey> orderDescExpr);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginatedWithBlocked(PaginatedItems<TEntity> paginatedItems, Expression<Func<TEntity, bool>> predicate);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginatedWithBlocked<TKey>(PaginatedItems<TEntity> paginatedItems, Expression<Func<TEntity, bool>> predicate, Func<TEntity, TKey> orderDescExpr);
        ProcessResult<TEntity> Get(params object[] id);
        ProcessResult<int> Count();
        ProcessResult<int> CountWithBlocked();
        ProcessResult<List<TEntity>> GetByIds(List<int> ids);
        ProcessResult<List<TEntity>> GetByIdsWithBlocked(List<int> ids);
        ProcessResult<TEntity> Get(Expression<Func<TEntity, bool>> predicate);
        ProcessResult<TEntity> Add(TEntity entity);
        ProcessResult<List<TEntity>> Add(List<TEntity> entityLst);
        ProcessResult<bool> Update(TEntity entity);
        ProcessResult<bool> Update(List<TEntity> entityLst);
        ProcessResult<bool> Delete(TEntity entity);
        ProcessResult<bool> LogicalDelete(TEntity entity);
        ProcessResult<bool> Delete(List<TEntity> entitylst);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginated(PaginatedItems<TEntity> paginatedItems);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginated<TKey>(PaginatedItems<TEntity> paginatedItems, Expression<Func<TEntity, bool>> predicate, Func<TEntity, TKey> orderDescExpr);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginated(PaginatedItems<TEntity> paginatedItems, Expression<Func<TEntity, bool>> predicate);
        ProcessResult<bool> DeleteById(params object[] id);
        ProcessResult<bool> LogicalDeleteById(params object[] id);

        int SaveChanges();
    }
}
