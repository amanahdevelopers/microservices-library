﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System.Linq.Expressions;
using System.Reflection;
using Utilites.ProcessingResult;
using Utilites.PaginatedItems;

namespace  DispatchProduct.RepositoryModule
{
    public class IdentityRepository<T> : IIdentityRepository<T> where T : class, IIdentityBaseEntity
    {
        private DbContext _context;

        public DbContext Context
        {
            get { return _context; }
        }
        private DbSet<T> _set;
        public IdentityRepository(DbContext context)
        {
            _context = context;
            _set = _context.Set<T>();
        }
        public virtual ProcessResult<IQueryable<T>> GetAllQuerable()
        {
            IQueryable<T> data = null;
            try
            {
                data = _set.Where(x => x.IsDeleted == false).AsNoTracking();
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual ProcessResult<List<T>> GetAll()
        {
            List<T> data = null;
            try
            {
                data = _set.Where(x => x.IsDeleted == false).AsNoTracking().ToList();
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }


        public virtual ProcessResult<T> Get(params object[] id)
        {
            T data = null;
            try
            {
                data = _set.Find(id);
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public ProcessResult<T> Get(Expression<Func<T, bool>> predicate)
        {
            T data = null;
            try
            {
                data = _set.FirstOrDefault(predicate);
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual ProcessResult<T> Add(T entity)
        {
            T data = null;
            try
            {
                if (Validator.IsValid(entity))
                {
                    entity.IsDeleted = false;
                    entity = _set.Add(entity).Entity;
                    if (SaveChanges() > 0)
                    {
                        data = entity;
                    }
                    return ProcessResultHelper.Succedded(entity);
                }
                else
                {
                    StringBuilder exceptionMsgs = new StringBuilder();
                    List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                    foreach (var errmsg in errorMsgs)
                    {
                        exceptionMsgs.Append(errmsg);
                        exceptionMsgs.Append("/n");
                    }
                    var ex = new Exception(exceptionMsgs.ToString());
                    return ProcessResultHelper.Failed(data, ex);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual ProcessResult<List<T>> Add(List<T> entityLst)
        {
            List<T> data = null;
            try
            {
                for (int i = 0; i < entityLst.Count; i++)
                {
                    var addresult = Add(entityLst[i]);
                    if (addresult.IsSucceeded)
                    {
                        entityLst[i] = addresult.Data;
                    }
                }
                data = entityLst;
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual ProcessResult<bool> Update(T entity)
        {
            bool data = false;
            try
            {
                if (Validator.IsValid(entity))
                {
                    _context.Entry<T>(entity).State = EntityState.Modified;
                    if (SaveChanges() > 0)
                    {
                        data = true;
                    }
                    return ProcessResultHelper.Succedded(data);
                }
                else
                {
                    StringBuilder exceptionMsgs = new StringBuilder();
                    List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                    foreach (var errmsg in errorMsgs)
                    {
                        exceptionMsgs.Append(errmsg);
                        exceptionMsgs.Append("/n");
                    }
                    var ex = new Exception(exceptionMsgs.ToString());
                    return ProcessResultHelper.Failed(data, ex);
                }
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual ProcessResult<bool> Update(IEnumerable<T> entityLst)
        {
            bool data = false;
            try
            {
                foreach (var entity in entityLst)
                {
                    if (Validator.IsValid(entity))
                    {

                        _context.Entry<T>(entity).State = EntityState.Modified;
                    }
                    else
                    {
                        StringBuilder exceptionMsgs = new StringBuilder();
                        List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                        foreach (var errmsg in errorMsgs)
                        {
                            exceptionMsgs.Append(errmsg);
                            exceptionMsgs.Append("/n");
                        }
                        var ex = new Exception(exceptionMsgs.ToString());
                        return ProcessResultHelper.Failed(data, ex);
                    }
                    if (SaveChanges() > 0)
                    {
                        data = true;
                    }
                }
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual ProcessResult<bool> Delete(T entity)
        {
            bool data = false;
            try
            {
                _context.Entry<T>(entity).State = EntityState.Deleted;
                data = _context.SaveChanges() > 0;
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual ProcessResult<bool> DeleteById(params object[] id)
        {
            T entity = _set.Find(id);
            return Delete(entity);
        }
        public virtual ProcessResult<bool> Delete(List<T> entitylst)
        {
            bool data = false;
            try
            {
                if (entitylst != null && entitylst.Count > 0)
                {
                    foreach (var entity in entitylst)
                    {
                        var deleteResult = Delete(entity);
                        if (!deleteResult.IsSucceeded)
                        {
                            return deleteResult;
                        }
                    }
                    data = true;
                }
                else
                {
                    data = true;
                }
                return ProcessResultHelper.Succedded(data);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(data, ex);
            }
        }
        public virtual int SaveChanges()
        {
            // this method handle any exception so no need to put it in try
            BaseEntityManager.AddAuditingData(_context.ChangeTracker.Entries());
            return _context.SaveChanges();
        }
        public T Get(string property, object value)
        {
            var lambda = CreateEqualSingleExpression(property, value);

            return _set.SingleOrDefault(lambda);
        }

        public Expression<Func<T, bool>> CreateEqualSingleExpression(string property, object value)
        {

            //p
            var p = Expression.Parameter(typeof(T));

            //p.Property
            var propertyExpression = Expression.Property(p, property);

            //p.Property == value
            var equalsExpression = Expression.Equal(propertyExpression, Expression.Constant(value));

            //p => p.Property == value
            return Expression.Lambda<Func<T, bool>>(equalsExpression, p);
        }

    }


}
