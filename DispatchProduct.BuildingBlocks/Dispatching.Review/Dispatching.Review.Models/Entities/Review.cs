﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Reviewing.Models.Entities
{
    public class Review:BaseEntity,IReview
    {
        public string FK_Reviewer_Id  { get; set; }
        public string Fk_Reviewed_Id { get; set; }
        public string Comment { get; set; }
        public int FK_LKP_Item_Id { get; set; }
        public int FK_LKP_Rating_Scale_Id { get; set; }
        public LKP_Item LKP_Item { get; set; }
        public LKP_Rating_Scale LKP_Rating_Scale { get; set; }
    }
}
