﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.CustomerModule.BLL.Managers;
using DispatchProduct.Controllers;
using Dispatching.Reviewing.Models.Entities;
using Utilites.ProcessingResult;
using Dispatching.Reviewing.API.ViewModels;

namespace Dispatching.Reviewing.API.Controllers
{
    //[Authorize]
    [Route("api/LKP_Rating_Scale")]
    public class LKP_Rating_ScaleController : BaseController<ILKP_Rating_ScaleManager, LKP_Rating_Scale, LKP_Rating_ScaleViewModel>
    {
        public LKP_Rating_ScaleController(ILKP_Rating_ScaleManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) :base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
           
        }
    }
}