﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.CustomerModule.BLL.Managers;
using DispatchProduct.Controllers;
using Dispatching.Reviewing.Models.Entities;
using Utilites.ProcessingResult;
using Dispatching.Reviewing.API.ViewModels;

namespace Dispatching.Reviewing.API.Controllers
{
    //[Authorize]
    [Route("api/LKP_Item")]
    public class LKP_ItemController : BaseController<ILKP_ItemManager, LKP_Item, LKP_ItemViewModel>
    {
        public LKP_ItemController(ILKP_ItemManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) :base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
           
        }
    }
}