﻿
using DispatchProduct.Repoistry;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dispatching.Reviewing.Models.Context;
using Dispatching.Reviewing.Models.Entities;

namespace DispatchProduct.CustomerModule.API.Seed
{
    public class ReviewDbContextSeed : ContextSeed
    {
        public async Task SeedAsync(ReviewDbContext context, IHostingEnvironment env,
            ILogger<ReviewDbContextSeed> logger,  int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                List<LKP_Item> lstLKP_Item = new List<LKP_Item>();
                List<LKP_Rating_Scale> lstLKP_Rating_Scale = new List<LKP_Rating_Scale>();
                lstLKP_Item = GetDefaultLKP_Items();
                lstLKP_Rating_Scale = GetDefaultLKP_Rating_Scale();
              
                await SeedEntityAsync(context, lstLKP_Item);

                await SeedEntityAsync(context, lstLKP_Rating_Scale);

            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for CustomerDbContextSeed");

                    await SeedAsync(context, env, logger, retryForAvaiability);
                }
            }
        }
        private List<LKP_Item> GetDefaultLKP_Items()
        {
            List<LKP_Item> result = new List<LKP_Item>
            {
                new LKP_Item() { Name = "Order" },
                new LKP_Item() { Name = "Application" }
            };
            return result;
        }

        private List<LKP_Rating_Scale> GetDefaultLKP_Rating_Scale()
        {
            List<LKP_Rating_Scale> result = new List<LKP_Rating_Scale>
            {
                new LKP_Rating_Scale() { Name = "Good" ,Degree=5},
                new LKP_Rating_Scale() { Name = "Very Good",Degree=7 },
                new LKP_Rating_Scale() { Name = "Bad",Degree=0 },
                new LKP_Rating_Scale() { Name = "Excellent",Degree=10 }
            };

            return result;
        }

      

    }
}
