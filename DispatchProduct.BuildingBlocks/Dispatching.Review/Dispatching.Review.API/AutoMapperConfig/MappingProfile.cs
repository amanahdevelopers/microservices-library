﻿using AutoMapper;
using Dispatching.Reviewing.API.ViewModels;
using Dispatching.Reviewing.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;

namespace Dispatching.Reviewing.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<LKP_Item, LKP_ItemViewModel>();
            CreateMap<LKP_ItemViewModel, LKP_Item>();

            CreateMap<LKP_Rating_Scale, LKP_Rating_ScaleViewModel>();
            CreateMap<LKP_Rating_ScaleViewModel, LKP_Rating_Scale>();

            CreateMap<Review, ReviewViewModel>();
            CreateMap<ReviewViewModel, Review>();

            

            CreateMap(typeof(PaginatedItems<>), typeof(PaginatedItemsViewModel<>));
            CreateMap(typeof(PaginatedItemsViewModel<>), typeof(PaginatedItems<>));

        }
    }
}
