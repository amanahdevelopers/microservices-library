﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.BuildingBlocks.BaseEntities.Entities
{
    public class BaseLKPEntity : BaseEntity, IBaseLKPEntity
    {
        public string NameAR { get; set; }
        public string NameEN { get; set; }
        public bool? IsSystemKey { get; set; }
    }
}
