﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DispatchProduct.BuildingBlocks.BaseEntities.Entities
{
    public class RepoistryBaseEntity : IRepoistryBaseEntity
    {
        //public virtual bool IsDeleted { get; set; }
        public virtual int Id { get; set; }
        public virtual string CurrentUserId { get; set; }

    }
}
