﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Utilites;

namespace Utilities.Utilites.GenericListToExcel
{
    public static class ListToExcelHelper
    {
        public static string WriteObjectsToExcel<T>(List<T> objects, string filePath) where T : new()
        {
            var properties = typeof(T).GetProperties();
            T obj = new T();

            FileUtilities.DeleteFileIfExist(filePath);
            FileStream stream = new FileStream(filePath, FileMode.OpenOrCreate);
            ExcelWriter writer = new ExcelWriter(stream);
            //writer.BeginWrite();
            for (int row = 0; row < objects.Count + 1; row++)
            {
                if (row != 0)
                {
                    obj = objects.Skip(row - 1).Take(1).FirstOrDefault();
                }
                for (int col = 0; col < properties.Length; col++)
                {
                    if (row == 0)
                    {
                        writer.WriteCell(row, col, properties[col].Name);
                    }
                    else
                    {
                        object value = obj.GetType().GetProperty(properties[col].Name).GetValue(obj);
                        if (value != null)
                        {
                            writer.WriteCell(row, col, value.ToString());
                        }
                        else
                        {
                            writer.WriteCell(row, col, "");
                        }
                    }
                }
                writer.NewLine();
            }
            writer.EndWriteFlush();
            stream.Close();
            return filePath;
        }
    }
}
