﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Utilites.UploadFile
{
    public class UploadImageFileManager : UploadFileManager, IUploadImageFileManager
    {
        List<string> imageExtensions = new List<string>() { ".png" , ".bmp" , ".jpg", ".jpeg",".tif",".gif" };
        public override ProcessResult<string> AddFile(UploadFile file, string filePath)
        {
            ProcessResult<string> result = new ProcessResult<string>();
            try
            {

                var isFileNotNull = ISFileNull(file);
                if (isFileNotNull.IsSucceeded)
                {

                    var extension = Path.GetExtension(file.FileName).ToLower();
                    if (imageExtensions.Contains(extension))
                    {
                        result = base.AddFile(file, filePath);
                    }
                    else
                    {

                        string message = "File must be of image extension";
                        var ex = new FileFormatException(message);
                        result=ProcessResultHelper.Failed("", ex, message, ProcessResultStatusCode.NotSupportedExtension);

                    }
                }
                else
                {
                    result = isFileNotNull;
                }

            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSucceeded = false;
            }
            return result;
        }
    }
}
