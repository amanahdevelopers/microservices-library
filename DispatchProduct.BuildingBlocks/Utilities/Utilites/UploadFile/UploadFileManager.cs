﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Utilites;
using Utilites.ProcessingResult;
using Utilities.ProcessingResult;

namespace Utilites.UploadFile
{
    public class UploadFileManager : IUploadFileManager
    {
        public virtual ProcessResult<string> AddFile(UploadFile file, string path)
        {
            ProcessResult<string> result = null;
            try
            {
                var isFileNotNull = ISFileNull(file);
                if (isFileNotNull.IsSucceeded)
                {
                    string fileBase64 = file.FileContent.Clone().ToString();
                    if (fileBase64 != null)
                    {
                        FileUtilities.CreateIfMissing(path);
                        string filePath = Path.Combine(path, file.FileName);
                        FileUtilities.WriteFileToPath(fileBase64, filePath);
                        file.FileRelativePath = filePath;
                        result = ProcessResultHelper.Succedded(path);
                    }
                }
                else
                {
                    result = isFileNotNull;
                }

            }
            catch (Exception ex)
            {
                result = ProcessResultHelper.Failed<string>(ex);
            }
            return result;
        }
        public virtual ProcessResult<List<string>> AddFiles(List<UploadFile> files, string path)
        {
            ProcessResult<List<string>> result = new ProcessResult<List<string>>();
            var data = new List<string>();
            foreach (var item in files)
            {
                var itemResult = AddFile(item, path);
                if (itemResult.IsSucceeded)
                {
                    data.Add(itemResult.Data);
                }
                else
                {
                    result = ProcessResultHelper.MapToProcessResult(itemResult, result);
                    break;
                }
            }
            result = ProcessResultHelper.Succedded(data);
            return result;
        }
        public virtual ProcessResult<string> ISFileNull(UploadFile file)
        {
            ProcessResult<string> result = null;
            if (file != null && file.FileName != null && file.FileContent != null)
            {
                result = ProcessResultHelper.Succedded("");
            }
            else
            {
                string message = "File can't be null, Please Insert valid File";
                var ex = new ArgumentNullException("File can't be null, Please Insert valid File");
                result = ProcessResultHelper.Failed("", ex, message, ProcessResultStatusCode.MissingArguments);
            }
            return result;

        }
    }
}
