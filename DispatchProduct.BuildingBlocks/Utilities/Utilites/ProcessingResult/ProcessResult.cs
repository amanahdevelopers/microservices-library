﻿using AutoMapper;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using Utilites.PaginatedItems;
using Utilites.PaginatedItemsViewModel;
using Utilities.ProcessingResult;

namespace Utilites.ProcessingResult
{
    public class ProcessResult<T> : IProcessResult<T>
    {
        public ProcessResult(string methodName)
        {
            MethodName = methodName;
        }
        public ProcessResult()
        {

        }
        public bool IsSucceeded { get; set; }
        public string MethodName { get; set; }
        public ProcessResultStatus Status { get; set; }
        public Exception Exception { get; set; }
        public T Data { get; set; }
    }
    public class ProcessResultMapper : IProcessResultMapper
    {
        public readonly IMapper mapper;
        public ProcessResultMapper(IMapper _mapper)
        {
            mapper = _mapper;
        }
        public ProcessResultViewModel<T2> Map<T1, T2>(ProcessResult<T1> input, ProcessResultViewModel<T2> output = null)
        {

            if (output == null)
            {
                output = new ProcessResultViewModel<T2>();
            }
            //if (Helper.IsValueType(output.Data))
            //{
            //    output.Data = input.Data;
            //}
            if (output.Data != null)
            {
                output.Data = mapper.Map(input.Data, output.Data);
            }
            else
            {
                output.Data = mapper.Map<T1, T2>(input.Data);
            }
            return ProcessResultHelper.MapProcessResult(input, output);
        }
    }
    public class ProcessResultPaginatedMapper : IProcessResultPaginatedMapper
    {
        public readonly IMapper mapper;
        public ProcessResultPaginatedMapper(IMapper _mapper)
        {
            mapper = _mapper;
        }
        public ProcessResultViewModel<PaginatedItemsViewModel<T2>> MapModel<T1, T2>(ProcessResult<PaginatedItems<T1>> input, ProcessResultViewModel<PaginatedItemsViewModel<T2>> output = null)
            where T1 : IRepoistryBaseEntity
            where T2 : IRepoistryBaseEntity
        {
            if (output == null)
            {
                output = new ProcessResultViewModel<PaginatedItemsViewModel<T2>>();
            }
            output.Data = MapModel(input.Data, output.Data);
            output = ProcessResultHelper.BindStatus(input, output);
            return ProcessResultHelper.MapProcessResult(input, output);
        }
        public ProcessResult<PaginatedItems<T2>> MapModel<T1, T2>(ProcessResultViewModel<PaginatedItemsViewModel<T1>> input, ProcessResult<PaginatedItems<T2>> output = null)
          where T1 : IRepoistryBaseEntity
          where T2 : IRepoistryBaseEntity
        {
            if (output == null)
            {
                output = new ProcessResult<PaginatedItems<T2>>();
            }
            output.Data = MapModel(input.Data, output.Data);
            output = ProcessResultHelper.BindStatus(input, output);
            return ProcessResultHelper.MapProcessResult(input, output);
        }

        public ProcessResultViewModel<PaginatedItemsViewModel<T2>> MapModel<T1, T2>(ProcessResultViewModel<PaginatedItemsViewModel<T1>> input, ProcessResultViewModel<PaginatedItemsViewModel<T2>> output = null)
          where T1 : IRepoistryBaseEntity
          where T2 : IRepoistryBaseEntity
        {
            if (output == null)
            {
                output = new ProcessResultViewModel<PaginatedItemsViewModel<T2>>();
            }
            output.Data = MapModel(input.Data, output.Data);
            output = ProcessResultHelper.BindStatus(input, output);
            output.Data = ProcessResultViewModelHelper.MapPaginatedItemsData(input.Data, output.Data, mapper);
            return output;
        }
        public PaginatedItemsViewModel<T2> MapModel<T1, T2>(PaginatedItemsViewModel<T1> input, PaginatedItemsViewModel<T2> output = null)
           where T1 : IRepoistryBaseEntity
           where T2 : IRepoistryBaseEntity
        {

            if (output != null)
            {
                output = mapper.Map(input, output);
                output = ProcessResultViewModelHelper.MapPaginatedItemsData(input, output, mapper);

            }
            else
            {
                output = mapper.Map<PaginatedItemsViewModel<T1>, PaginatedItemsViewModel<T2>>(input);
                output = ProcessResultViewModelHelper.MapPaginatedItemsData(input, output, mapper);

            }
            return output;
        }
        public PaginatedItemsViewModel<T2> MapModel<T1, T2>(PaginatedItems<T1> input, PaginatedItemsViewModel<T2> output = null)
           where T1 : IRepoistryBaseEntity
           where T2 : IRepoistryBaseEntity
        {

            if (output != null)
            {
                output = mapper.Map(input, output);
                output = ProcessResultHelper.MapPaginatedItemsData(input, output, mapper);

            }
            else
            {
                output = mapper.Map<PaginatedItems<T1>, PaginatedItemsViewModel<T2>>(input);
                output = ProcessResultHelper.MapPaginatedItemsData(input, output, mapper);

            }
            return output;
        }
        public PaginatedItems<T2> MapModel<T1, T2>(PaginatedItemsViewModel<T1> input, PaginatedItems<T2> output = null)
          where T1 : IRepoistryBaseEntity
          where T2 : IRepoistryBaseEntity
        {
            if (output != null)
            {
                output = mapper.Map(input, output);
                output = ProcessResultHelper.MapPaginatedItemsData(input, output, mapper);
            }
            else
            {
                output = mapper.Map<PaginatedItemsViewModel<T1>, PaginatedItems<T2>>(input);
                output = ProcessResultHelper.MapPaginatedItemsData(input, output, mapper);
            }
            return output;
        }

    }
    public interface IProcessResultMapper
    {
        ProcessResultViewModel<T2> Map<T1, T2>(ProcessResult<T1> input, ProcessResultViewModel<T2> output = null);
    }
    public interface IProcessResultPaginatedMapper
    {
        ProcessResultViewModel<PaginatedItemsViewModel<T2>> MapModel<T1, T2>(ProcessResult<PaginatedItems<T1>> input, ProcessResultViewModel<PaginatedItemsViewModel<T2>> output = null)
             where T1 : IRepoistryBaseEntity
            where T2 : IRepoistryBaseEntity;
        ProcessResult<PaginatedItems<T2>> MapModel<T1, T2>(ProcessResultViewModel<PaginatedItemsViewModel<T1>> input = null, ProcessResult<PaginatedItems<T2>> output = null)
             where T1 : IRepoistryBaseEntity
            where T2 : IRepoistryBaseEntity;
        PaginatedItemsViewModel<T2> MapModel<T1, T2>(PaginatedItems<T1> input, PaginatedItemsViewModel<T2> output = null)
         where T1 : IRepoistryBaseEntity
         where T2 : IRepoistryBaseEntity;
        PaginatedItems<T2> MapModel<T1, T2>(PaginatedItemsViewModel<T1> input = null, PaginatedItems<T2> output = null)
         where T1 : IRepoistryBaseEntity
         where T2 : IRepoistryBaseEntity;
        ProcessResultViewModel<PaginatedItemsViewModel<T2>> MapModel<T1, T2>(ProcessResultViewModel<PaginatedItemsViewModel<T1>> input, ProcessResultViewModel<PaginatedItemsViewModel<T2>> output = null)
             where T1 : IRepoistryBaseEntity
         where T2 : IRepoistryBaseEntity;
    }
    public static class ProcessResultFactory
    {
        public static ProcessResult<T> Create<T>(T input)
        {
            var result = new ProcessResult<T>();
            result.Status = new ProcessResultStatus();
            result.Data = input;
            return result;
        }
        public static ProcessResultViewModel<T> CreateViewModel<T>(T input)
        {
            var result = new ProcessResultViewModel<T>();
            result.Status = new ProcessResultStatus();
            result.Data = input;
            return result;
        }
    }
    public static class ProcessResultHelper
    {
        public static PaginatedItems<T2> MapPaginatedItemsData<T1, T2>(PaginatedItemsViewModel<T1> inputData, PaginatedItems<T2> outputData, IMapper mapper)
          where T1 : IRepoistryBaseEntity
          where T2 : IRepoistryBaseEntity
        {
            if (outputData != null)
            {
                if (outputData.Data == null)
                {
                    outputData.Data = mapper.Map<List<T1>, List<T2>>(inputData.Data);
                }
                else
                {
                    outputData.Data = mapper.Map(inputData.Data, outputData.Data);
                }
            }
            return outputData;
        }
        public static PaginatedItemsViewModel<T2> MapPaginatedItemsData<T1, T2>(PaginatedItems<T1> inputData, PaginatedItemsViewModel<T2> outputData, IMapper mapper)
         where T1 : IRepoistryBaseEntity
         where T2 : IRepoistryBaseEntity
        {
            if (outputData != null)
            {
                if (outputData.Data == null)
                {
                    outputData.Data = mapper.Map<List<T1>, List<T2>>(inputData.Data);
                }
                else
                {
                    outputData.Data = mapper.Map(inputData.Data, outputData.Data);
                }
            }
            return outputData;
        }
        public static ProcessResult<T2> MapToProcessResult<T1, T2>(ProcessResult<T1> input, ProcessResult<T2> output = null)
        {
            if (output == null)
            {
                output = new ProcessResult<T2>();
            }
            BindStatus(input, output);
            return output;
        }
        public static ProcessResultViewModel<T2> BindStatus<T1, T2>(ProcessResultViewModel<T1> input, ProcessResultViewModel<T2> output = null)
        {
            if (output != null)
            {
                output.MethodName = input.MethodName;
                output.IsSucceeded = input.IsSucceeded;
                output.Status = input.Status;
            }
            return output;
        }
        public static ProcessResult<T2> BindStatus<T1, T2>(ProcessResult<T1> input, ProcessResult<T2> output = null)
        {
            if (output != null)
            {
                output.MethodName = input.MethodName;
                output.IsSucceeded = input.IsSucceeded;
                output.Status = input.Status;
                output.Exception = input.Exception;
            }
            return output;
        }
        public static ProcessResult<T2> BindStatus<T1, T2>(ProcessResultViewModel<T1> input, ProcessResult<T2> output = null)
        {
            if (output != null)
            {
                output.MethodName = input.MethodName;
                output.IsSucceeded = input.IsSucceeded;
                output.Status = input.Status;
            }
            return output;
        }
        public static ProcessResultViewModel<T2> BindStatus<T1, T2>(ProcessResult<T1> input, ProcessResultViewModel<T2> output = null)
        {
            if (output != null)
            {
                output.MethodName = input.MethodName;
                output.IsSucceeded = input.IsSucceeded;
                output.Status = input.Status;
            }
            return output;
        }
        public static ProcessResultViewModel<T2> MapProcessResult<T1, T2>(ProcessResult<T1> input, ProcessResultViewModel<T2> output = null)
        {
            if (output == null)
            {
                output = new ProcessResultViewModel<T2>();
            }
            BindStatus(input, output);
            return output;
        }
        public static ProcessResult<T2> MapProcessResult<T1, T2>(ProcessResultViewModel<T1> input, ProcessResult<T2> output = null)
        {
            if (output == null)
            {
                output = new ProcessResult<T2>();
            }
            BindStatus(input, output);
            return output;
        }
        public static ProcessResult<T> Succedded<T>(T input, string message = null, ProcessResultStatusCode statusCode = ProcessResultStatusCode.Succeded, [CallerMemberName] string method = null, int subCode = 0)
        {
            var result = FillPropsAutomatically(input, statusCode, method, message, subCode);
            result.IsSucceeded = true;
            if (string.IsNullOrEmpty(message))
                result.Status.Message = "Process Completed Successfully";
            return result;
        }
        public static ProcessResult<T> Failed<T>(T input, Exception ex, string message = null, ProcessResultStatusCode statusCode = ProcessResultStatusCode.Failed, [CallerMemberName] string method = null, int subCode = 0)
        {
            var result = FillPropsAutomatically(input, statusCode, method, message, subCode);
            result.IsSucceeded = false;
            result.Exception = ex;
            if (string.IsNullOrEmpty(message))
            {
                if (ex != null)
                    result.Status.Message = ex.Message;
                else
                    result.Status.Message = "Process Failed";
            }
            return result;
        }
        //public static ProcessResult<T> Failed<T>(T input, Exception ex, string message = null, ProcessResultStatusCode statusCode = ProcessResultStatusCode.Failed, [CallerMemberName] string method = null)
        //=> Failed(input, ex, 0, message, statusCode);
        public static ProcessResult<T> Failed<T>(Exception ex, T input = default)
        => Failed<T>(input, ex, "", ProcessResultStatusCode.Failed, subCode: 0);


        public static ProcessResult<T> FillPropsAutomatically<T>(T input, ProcessResultStatusCode statusCode, string method, string message = null, int subCode = 0)
        {
            var result = ProcessResultFactory.Create(input);
            result.Data = input;
            result.MethodName = method;
            result.Status.Code = statusCode;
            result.Status.SubCode = subCode;
            if (!string.IsNullOrEmpty(message))
                result.Status.Message = message;
            return result;
        }
    }
    public static class ProcessResultViewModelHelper
    {
        public static PaginatedItemsViewModel<T2> MapPaginatedItemsData<T1, T2>(PaginatedItemsViewModel<T1> inputData, PaginatedItemsViewModel<T2> outputData, IMapper mapper)
          where T1 : IRepoistryBaseEntity
          where T2 : IRepoistryBaseEntity
        {
            if (outputData != null)
            {
                if (outputData.Data == null)
                {
                    outputData.Data = mapper.Map<List<T1>, List<T2>>(inputData.Data);
                }
                else
                {
                    outputData.Data = mapper.Map(inputData.Data, outputData.Data);
                }
            }
            return outputData;
        }
        public static ProcessResultViewModel<T2> MapToProcessResultViewModel<T1, T2>(ProcessResultViewModel<T1> input, ProcessResultViewModel<T2> output = null)
        {
            if (output == null)
            {
                output = new ProcessResultViewModel<T2>();
            }
            output.MethodName = input.MethodName;
            output.IsSucceeded = input.IsSucceeded;
            output.Status = input.Status;
            return output;
        }
        public static ProcessResultViewModel<T> Succedded<T>(T input, string message = null, ProcessResultStatusCode statusCode = ProcessResultStatusCode.Succeded, [CallerMemberName] string method = null, int subCode = 0)
        {
            var result = FillPropsAutomatically(input, statusCode, method, message, subCode);
            result.IsSucceeded = true;
            if (string.IsNullOrEmpty(message))
                result.Status.Message = "Process Completed Successfully";
            return result;
        }
        public static ProcessResultViewModel<T> Failed<T>(T input, string message = null, ProcessResultStatusCode statusCode = ProcessResultStatusCode.Failed, [CallerMemberName] string method = null, int subCode = 0)
        {
            var result = FillPropsAutomatically(input, statusCode, method, message, subCode);
            result.IsSucceeded = false;
            if (string.IsNullOrEmpty(message))
                result.Status.Message = "Process Failed";
            return result;
        }
        public static ProcessResultViewModel<T> FillPropsAutomatically<T>(T input, ProcessResultStatusCode statusCode, string method, string message = null, int subCode = 0)
        {
            var result = ProcessResultFactory.CreateViewModel(input);
            result.Data = input;
            result.MethodName = method;
            result.Status.Code = statusCode;
            result.Status.SubCode = subCode;
            if (!string.IsNullOrEmpty(message))
                result.Status.Message = message;
            return result;
        }

    }
}
