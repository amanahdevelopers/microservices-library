﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.ProcessingResult;

namespace Utilites.ProcessingResult
{
    public class ProcessResultStatus: IProcessResultStatus
    {
        public ProcessResultStatusCode Code { get; set; }
        public int SubCode { get; set; }
        public string Message { get; set; }
    }
}
