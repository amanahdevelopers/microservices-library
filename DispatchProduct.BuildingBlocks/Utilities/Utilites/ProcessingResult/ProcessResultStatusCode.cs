﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.ProcessingResult
{
    public enum ProcessResultStatusCode
    {
        DefaultValue = 0,
        Succeded = 1,
        Failed = 2,
        NotFound = 3,
        MissingArguments = 4,
        NullArguments=5,
        NotSupportedExtension=6,
        InvalidValue=6,
        TeamNameExistedBefore=7,
        CanNotChangePF=8,
        thisSuperviosrHasDispatchers=9,
        ThisSupervisorHasOrders = 10,
        YouCantChangeThisForemanDivision = 11,
        ThisDispatcherHasFormans = 12,
        thisDispatcherHasCurrentOrdersAndHasForemans = 16,
        ThisDispatcherHasOrders = 18,
        YouCantChangeThisTechnicianDivision = 13,
        thisEngineerHasForemans=14,
        thisSupervisorHasOrdersAndHasDispatchers=15,
        ThisForemanBelongsToTeam = 17,
        ThisTechnicianBelongsToTeam =19,
        ThisManagerHasSupervisors=20
       


    };
}
