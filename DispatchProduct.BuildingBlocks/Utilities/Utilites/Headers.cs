﻿using CommonEnums;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Utilities
{
    public class Headers
    {
        public static ProcessResultViewModel<SupportedLanguage> GetSupportedLanguage(HttpRequest request)
        {
            ProcessResultViewModel<SupportedLanguage> result = null;
            object data;
            var SupportedLanguageHeader = request.Headers["Accept-Language"].ToString();
            if (!string.IsNullOrEmpty(SupportedLanguageHeader))
            {
                Enum.TryParse(typeof(SupportedLanguage), SupportedLanguageHeader, out data);
                if (data != null && data is SupportedLanguage)
                {
                    var supportedLanguage = (SupportedLanguage)data;
                    if (supportedLanguage != 0)
                    {
                        result = ProcessResultViewModelHelper.Succedded(supportedLanguage);
                    }
                    else
                    {
                        result = ProcessResultViewModelHelper.Failed<SupportedLanguage>(0, message: "SupportedLanguage Header value not supported");
                    }
                }
                else
                {
                    result = ProcessResultViewModelHelper.Failed<SupportedLanguage>(0, message: "SupportedLanguage Header value not supported");
                }

            }
            else
            {
                result = ProcessResultViewModelHelper.Failed<SupportedLanguage>(0, message: "SupportedLanguage Header Requst not Defined");
            }
            return result;
        }
    }
}
