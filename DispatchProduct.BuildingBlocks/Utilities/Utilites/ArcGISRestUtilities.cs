﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Utilites
{
    public static class ArcGISRestUtilities
    {
        public static string ConstructQueryGetURL(string proxyUrl, string serviceUrl, Dictionary<string,object> parametersValuesDict)

        {
            string whereClause = ConstructWhereClause(parametersValuesDict);
            var query = String.Format("query?{0}&returnGeometry=false&outFields=*&f=pjson", whereClause);
            var URL = String.Format("{0}?{1}/{2}", proxyUrl, serviceUrl, query);
            return URL;
        }
        
        private static string ConstructWhereClause(Dictionary<string, object> parametersValuesDict)
        {
            string result = "where=";
            foreach (var key in parametersValuesDict.Keys)
            {
                object value = parametersValuesDict[key];
                if (value is int intValue)
                {
                    if (parametersValuesDict.Keys.First().Equals(key))
                    {
                        result += string.Format("{0}={1}", key, intValue);
                    }
                    else
                    {
                        result += string.Format(" and {0}={1}", key, intValue);
                    }
                }
                else
                {
                    if (parametersValuesDict.Keys.First().Equals(key))
                    {
                        result += string.Format("{0}='{1}'", key, value.ToString());
                    }
                    else
                    {
                        result += string.Format(" and {0}='{1}'", key, value.ToString());
                    }
                }
            }
            return result;
        }
    }
}
