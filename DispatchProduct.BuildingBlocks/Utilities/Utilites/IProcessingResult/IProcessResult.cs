﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Utilites.NewtonsoftHelper;

namespace Utilites.ProcessingResult
{
    public interface IProcessResult<T>
    {
        bool IsSucceeded { get; set; }
        string MethodName { get; set; }
        ProcessResultStatus Status { get; set; }
        Exception Exception { get; set; }
        T Data { get; set; }
    }
}
