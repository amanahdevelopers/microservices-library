﻿using System;
using System.Collections.Generic;

namespace ExcelToGenericList
{
    public class Product
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int? CategoryID { get; set; }
        public Decimal? UnitPrice { get; set; }                
        public bool OutOfStock { get; set; }
        public DateTime? StockDate { get; set; }
        public string Brand { get; set; }

        //public Guid? Code { get; set; }
    }
}