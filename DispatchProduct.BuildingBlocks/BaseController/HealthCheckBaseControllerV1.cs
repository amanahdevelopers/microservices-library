﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.RepositoryModule;
using AutoMapper;
using Utilites.ProcessingResult;
using Utilites.PaginatedItems;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Utilites.PaginatedItemsViewModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class HealthCheckBaseControllerV1 : HealthCheckBaseController
    {
        public HealthCheckBaseControllerV1()
        {

        }

        [HttpGet(""), MapToApiVersion("1.0")]
        [HttpHead("")]
        public override IActionResult Ping()
        {
            return base.Ping();
        }
    }
}
