﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.RepositoryModule;
using AutoMapper;
using Utilites.ProcessingResult;
using Utilites.PaginatedItems;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Utilites.PaginatedItemsViewModel;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq.Expressions;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Controllers
{
    public class BaseController<ITManager, T, TViewModel> : Controller
        where ITManager : IRepository<T>
        where TViewModel : class, IRepoistryBaseEntity
        where T : class, IBaseEntity
    {
        public ITManager manger;
        public readonly IMapper mapper;
        IProcessResultMapper processResultMapper;
        IProcessResultPaginatedMapper processResultPaginatedMapper;
        protected string CurrentUserId { get; set; }
        public BaseController(ITManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            processResultMapper = _processResultMapper;
            processResultPaginatedMapper = _processResultPaginatedMapper;

        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        [HttpGet]
        public virtual ProcessResultViewModel<TViewModel> Get([FromRoute]int id)
        {
            var entityResult = manger.Get(id);
            //FillCurrentUser(entityResult);
            return processResultMapper.Map<T, TViewModel>(entityResult);
        }

        [Route("Count")]
        [HttpGet]
        public virtual ProcessResultViewModel<int> Count ()
        {
            var entityResult = manger.Count();
            return processResultMapper.Map<int, int>(entityResult);
        }

        [Route("CountWithBlocked")]
        [HttpGet]
        public virtual ProcessResultViewModel<int> CountWithBlocked()
        {
            var entityResult = manger.CountWithBlocked();
            return processResultMapper.Map<int, int>(entityResult);
        }

        [Route("GetByIds")]
        [HttpPost]
        public virtual ProcessResultViewModel<List<TViewModel>> GetByIds([FromBody]List<int> ids)
        {
            var entityResult = manger.GetByIds(ids);
            //FillCurrentUser(entityResult);
            return processResultMapper.Map<List<T>, List<TViewModel>>(entityResult);
        }

        [Route("GetByIdsWithBlocked")]
        [HttpPost]
        public virtual ProcessResultViewModel<List<TViewModel>> GetByIdsWithBlocked([FromBody]List<int> ids)
        {
            var entityResult = manger.GetByIdsWithBlocked(ids);
            //FillCurrentUser(entityResult);
            return processResultMapper.Map<List<T>, List<TViewModel>>(entityResult);
        }

        [Route("GetAll")]
        [HttpGet]
        public virtual ProcessResultViewModel<List<TViewModel>> Get()
        {
            var entityResult = manger.GetAll();
            //FillCurrentUser(entityResult);
            return processResultMapper.Map<List<T>, List<TViewModel>>(entityResult);
        }
        [Route("GetAllWithBlocked")]
        [HttpGet]
        public virtual ProcessResultViewModel<List<TViewModel>> GetAllWithBlocked()
        {
            var entityResult = manger.GetAllWithBlocked();
            //FillCurrentUser(entityResult);
            return processResultMapper.Map<List<T>, List<TViewModel>>(entityResult);
        }

        //[Route("GetAllByPredicate")]
        //[HttpGet]
        //public virtual ProcessResultViewModel<List<TViewModel>> Get(Expression<Func<T, bool>> predicate = null)
        //{
        //    var entityResult = manger.GetAll(predicate);
        //    //FillCurrentUser(entityResult);
        //    return processResultMapper.Map<List<T>, List<TViewModel>>(entityResult);
        //}
        [Route("GetAllPaginated")]
        [HttpPost]
        public virtual ProcessResultViewModel<PaginatedItemsViewModel<TViewModel>> GetAllPaginated([FromBody]PaginatedItemsViewModel<TViewModel> model = null)
        {
            PaginatedItems<T> entityData = null;
            if (model == null)
            {
                entityData = new PaginatedItems<T>();
            }
            else
            {
                entityData = processResultPaginatedMapper.MapModel<TViewModel, T>((PaginatedItemsViewModel< TViewModel>)model);
                //FillCurrentUser(entityData.Data);

            }
            var entityResult = manger.GetAllPaginated(entityData);
            return processResultPaginatedMapper.MapModel<T, TViewModel>(entityResult);
        }
        [Route("GetAllPaginatedWithBlocked")]
        [HttpPost]
        public virtual ProcessResultViewModel<PaginatedItemsViewModel<TViewModel>> GetAllPaginatedWithBlocked([FromBody]PaginatedItemsViewModel<TViewModel> model = null)
        {
            PaginatedItems<T> entityData = null;
            if (model == null)
            {
                entityData = new PaginatedItems<T>();
            }
            else
            {
                entityData = processResultPaginatedMapper.MapModel<TViewModel, T>((PaginatedItemsViewModel<TViewModel>)model);
                //FillCurrentUser(entityData.Data);

            }
            var entityResult = manger.GetAllPaginatedWithBlocked(entityData);
            return processResultPaginatedMapper.MapModel<T, TViewModel>(entityResult);
        }

        [Route("GetAllPaginatedByPredicate")]
        [HttpPost]
        public virtual ProcessResultViewModel<PaginatedItemsViewModel<TViewModel>> GetAllPaginatedByPredicate([FromBody]PaginatedItemsViewModel<TViewModel> model = null, Expression<Func<T, bool>> predicate = null)
        {
            PaginatedItems<T> entityData = null;
            if (model == null)
            {
                entityData = new PaginatedItems<T>();
            }
            else
            {
                entityData = processResultPaginatedMapper.MapModel<TViewModel, T>((PaginatedItemsViewModel<TViewModel>)model);
                //FillCurrentUser(entityData.Data);

            }
            var entityResult = manger.GetAllPaginated(entityData, predicate);
            return processResultPaginatedMapper.MapModel<T, TViewModel>(entityResult);
        }

        [Route("GetAllPaginatedByPredicateWithBlocked")]
        [HttpPost]
        public virtual ProcessResultViewModel<PaginatedItemsViewModel<TViewModel>> GetAllPaginatedByPredicateWithBlocked([FromBody]PaginatedItemsViewModel<TViewModel> model = null, Expression<Func<T, bool>> predicate = null)
        {
            PaginatedItems<T> entityData = null;
            if (model == null)
            {
                entityData = new PaginatedItems<T>();
            }
            else
            {
                entityData = processResultPaginatedMapper.MapModel<TViewModel, T>((PaginatedItemsViewModel<TViewModel>)model);
                //FillCurrentUser(entityData.Data);

            }
            var entityResult = manger.GetAllPaginatedWithBlocked(entityData, predicate);
            return processResultPaginatedMapper.MapModel<T, TViewModel>(entityResult);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public virtual ProcessResultViewModel<TViewModel> Post([FromBody]TViewModel model)
        {
            //FillCurrentUser(model);
            var entityModel = mapper.Map<TViewModel, T>(model);
            var entityResult = manger.Add(entityModel);
            return processResultMapper.Map<T, TViewModel>(entityResult);
        }
        [HttpPost]
        [Route("AddMulti")]
        public virtual ProcessResultViewModel<List<TViewModel>> PostMulti([FromBody]List<TViewModel> lstmodel)
        {
             //FillCurrentUser(lstmodel);
             var entityModel = mapper.Map<List<TViewModel>, List<T>>(lstmodel);
             var entityResult = manger.Add(entityModel);
            return processResultMapper.Map<List<T>, List<TViewModel>>(entityResult);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public virtual ProcessResultViewModel<bool> Put([FromBody]TViewModel model)
        {
            FillCurrentUser(model);
            var entitymodel = mapper.Map<TViewModel, T>(model);
            var result = manger.Update(entitymodel);
            return processResultMapper.Map<bool, bool>(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]
        public virtual ProcessResultViewModel<bool> Delete([FromRoute]int id)
        {
            var result = manger.DeleteById(id);
            return processResultMapper.Map<bool, bool>(result);
        }
        #endregion

        #region LogicalDeleteApi
        [HttpDelete]
        [Route("LogicalDelete/{id}")]
        public virtual ProcessResultViewModel<bool> LogicalDelete([FromRoute]int id)
        {
            var result = manger.LogicalDeleteById(id);
            return processResultMapper.Map<bool, bool>(result);
        }
        #endregion

        #endregion

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
            //CurrentUserId = HttpContext.User.FindFirst("jti").Value;
        }
        private T FillCurrentUser(T model)
        {
            if (model != null
                &&
                !String.IsNullOrEmpty(CurrentUserId))
            {
                model.CurrentUserId = CurrentUserId;
            }
            return model;
        }
        private ProcessResult<T> FillCurrentUser(ProcessResult<T> model)
        {
            if (model.IsSucceeded)
            {
                FillCurrentUser(model.Data);
            }
            return model;
        }
        private ProcessResult<List<T>> FillCurrentUser(ProcessResult<List<T>> model)
        {
            for (int i = 0; i < model.Data.Count; i++)
            {
                model.Data[i] = FillCurrentUser(model.Data[i]);
            }
            return model;
        }
        private List<T> FillCurrentUser(List<T> model)
        {
            for (int i = 0; i < model.Count; i++)
            {
                model[i] = FillCurrentUser(model[i]);
            }
            return model;
        }

        private TViewModel FillCurrentUser(TViewModel model)
        {
            if (model != null && String.IsNullOrEmpty(CurrentUserId))
            {
                model.CurrentUserId = CurrentUserId;
            }
            return model;
        }

        private List<TViewModel> FillCurrentUser(List<TViewModel> model)
        {
            if (model != null)
            {
                for (int i = 0; i < model.Count; i++)
                {
                    model[i] = FillCurrentUser(model[i]);
                }
            }
            return model;
        }
    }
}
