﻿using VehicleAPI.Models;
using VehicleAPI.Outputs;
using VehicleAPI.Inputs;
using DispatchingSystem.Models;

namespace VehicleAPI.BL.Interface
{
    public interface IVT
    {
        DTO<GetVehiclesDTO> GetVehicles(Input<GetVehicles> obj, DataContext db);
        DTO<ShowVehiclesDTO> ShowVehicles(Input<ShowVehicles> obj, DataContext db);
        DTO<ShowVehiclesRegDTO> ShowVehiclesReg(Input<ShowVehiclesReg> obj, DataContext db);
        DTO<GetVehiclesLocationDTO> GetVehiclesLocation(Input<GetVehiclesLocation> obj, DataContext db);
    }
}
