﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.CustomerModule.BLL.Managers;
using DispatchProduct.Controllers;
using Dispatching.Reviewing.Models.Entities;
using Utilites.ProcessingResult;
using Dispatching.Reviewing.API.ViewModels;

namespace Dispatching.Reviewing.API.Controllers
{
    //[Authorize]
    [Route("api/Review")]
    public class ReviewController : BaseController<IReviewManager, Review, ReviewViewModel>
    {
        public ReviewController(IReviewManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper) :base(_manger, _mapper, _processResultMapper)
        {
           
        }
    }
}