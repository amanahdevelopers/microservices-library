﻿using Dispatching.Reviewing.Models.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Reviewing.Models.EntityConfigurations
{
    public class LKP_ItemEntityTypeConfiguration : BaseEntityTypeConfiguration<LKP_Item>
    {
        public void Configure(EntityTypeBuilder<LKP_Item> LKP_ItemConfiguration)
        {
            base.Configure(LKP_ItemConfiguration);

            LKP_ItemConfiguration.ToTable("LKP_Item");

            LKP_ItemConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("LKP_Itemseq");
            LKP_ItemConfiguration.Property(p => p.Id).ValueGeneratedOnAdd();
            LKP_ItemConfiguration.Property(o => o.Name).IsRequired();
            LKP_ItemConfiguration.HasKey(o => o.Id);
        }
    }
}